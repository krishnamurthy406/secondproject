package automation;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class firstTest 
{
	WebDriver driver;
	
  @Test
  public void f() throws InterruptedException 
  {
	  System.setProperty("webdriver.chrome.driver","C:\\Users\\krishna.bugeni\\Downloads\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.get("https://www.amazon.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@id='twotabsearchtextbox']")).sendKeys("The AIchemist book");
		driver.findElement(By.xpath("//input[@id='nav-search-submit-button']")).click();
		driver.findElement(By.xpath("//span[text()='The Alchemist, 25th Anniversary: A Fable About Following Your Dream']")).click();
		WebElement wb=driver.findElement(By.xpath("//span[text()='0062315005']"));
		String actualASIN=wb.getText();
		String expectedASIN="0062315005";
		if(actualASIN.equals(expectedASIN))
		{
			System.out.println("ASIN value is valid");
		}
		else
		{
			System.out.println("ASIN value is Invalid");
		}
		WebElement wb1=driver.findElement(By.xpath("//span[text()='HarperOne; 25th ed. edition (April 15, 2014)']"));
		String actualPublisher=wb1.getText();
		String expectPublisher="HarperOne; 25th ed. edition (April 15, 2014)";
		if(actualPublisher.equals(expectPublisher))
		{
			System.out.println("publisher  is valid");
		}
		else
		{
			System.out.println("publisher is Invalid");
		}
		WebElement wb2=driver.findElement(By.xpath("//span[text()='7.9 x 5.1 x 0.8 inches']"));
		String actualdimension=wb2.getText();
		String expectDimension="7.9 x 5.1 x 0.8 inches";
		if(actualdimension.equals(expectDimension))
		{
			System.out.println("Dimension is valid");
		}
		else
		{
			System.out.println("Dimension  is Invalid");
		}
		WebElement wb3=driver.findElement(By.xpath("//span[text()='6.1 ounces']"));
		String actualnetquality=wb2.getText();
		String expectnetquality="6.1 ounces";
		if(actualnetquality.equals(expectnetquality))
		{
			System.out.println("net quality is valid");
		}
		else
		{
			System.out.println("net quality is Invalid");
		}
		
  }
}
